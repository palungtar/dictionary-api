#!/usr/bin/env python3

from sys import argv
from urllib import request
from urllib import parse
from urllib import error
import json
import ssl

class WordDef:

    def __init__(self, word):
        self.word = word
        self.address = "https://www.dictionaryapi.com/api/v3/references/collegiate/json/{word}?".format(word=word)

    def get_api_key(self):
        with open("api_key.txt") as fh:
            api_key = fh.read().strip() 
        return api_key

    def get_definition(self, word):
        parms = dict()
        api_key = self.get_api_key()
        parms['key'] = api_key
        parms['address'] = self.address
        url = self.address + parse.urlencode(parms)
        uh = request.urlopen(url)
        data_dict_string = uh.read().decode()
        data_dict = json.loads(data_dict_string)[0]
        #`ek-sər-sīz` (noun): the act of bringing into play or realizing in action
        #print(type(data_dict))
        pronounciation = data_dict["hwi"]["prs"][0]["mw"]
        fl = data_dict["fl"]
        shortdef = data_dict["shortdef"][0]
        output = "`{pronounciation}` ({fl}): {shortdef}".format(
            fl=fl,
            pronounciation=pronounciation,
            shortdef = shortdef
        )
        print(output)
        #print('Retrieved', len(data), 'characters')
        #js = json.loads(data)
        #print(json.dumps(js, indent=4))
        return output

if __name__ == "__main__":
    script_name, word = argv
    word_def = WordDef(word)
    word_def.get_definition(word)
